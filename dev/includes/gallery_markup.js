// This is the glalery markup for PhotoSwipe

var gallery_markup = '';

	gallery_markup += '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">';
		gallery_markup += '<div class="pswp__bg"></div>';
		gallery_markup += '<div class="pswp__scroll-wrap">';
			gallery_markup += '<div class="pswp__container">';
				gallery_markup += '<div class="pswp__item"></div>';
				gallery_markup += '<div class="pswp__item"></div>';
				gallery_markup += '<div class="pswp__item"></div>';
			gallery_markup += '</div>';
			gallery_markup += '<div class="pswp__ui pswp__ui--hidden">';
				gallery_markup += '<div class="pswp__top-bar">';
					gallery_markup += '<div class="pswp__counter"></div>';
					gallery_markup += '<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>';
					gallery_markup += '<button class="pswp__button pswp__button--share" title="Share"></button>';
					gallery_markup += '<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>';
					gallery_markup += '<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>';
					gallery_markup += '<div class="pswp__preloader">';
						gallery_markup += '<div class="pswp__preloader__icn">';
						gallery_markup += '<div class="pswp__preloader__cut">';
							gallery_markup += '<div class="pswp__preloader__donut"></div>';
						gallery_markup += '</div>';
						gallery_markup += '</div>';
					gallery_markup += '</div>';
				gallery_markup += '</div>';
				gallery_markup += '<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">';
					gallery_markup += '<div class="pswp__share-tooltip"></div>';
				gallery_markup += '</div>';
				gallery_markup += '<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>';
				gallery_markup += '<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>';
				gallery_markup += '<div class="pswp__caption">';
					gallery_markup += '<div class="pswp__caption__center"></div>';
				gallery_markup += '</div>';
			gallery_markup += '</div>';
		gallery_markup += '</div>';
	gallery_markup += '</div>';