// JavaScript Document

// @codekit-prepend "frameworks/jquery/jquery-3.2.1.min.js";
// @codekit-prepend "frameworks/photoswipe/photoswipe.min.js";
// @codekit-prepend "frameworks/photoswipe/photoswipe-ui-default.min.js";
// @codekit-prepend "includes/gallery_markup.js";


// .................................................................................................
// vars.............................................................................................
var site_url = 'http://maxmadness.mymobileevents.com/';
var loaded_data,speakers,sessions,view_state = null;
var data_url = 'http://admin.mymobileevents.com/maxmadness/mme-data/';
var data_page = 'home';
var data_page = 'gallery';
var data_page_detail = 'none';
//var data_page_detail = 'another-sample-session';
//var data_page_detail = 'russell-brown';





// .................................................................................................
// home page .......................................................................................
function load_home_content(){
	var markup = home;
	$('.list').html(markup);
}





// .................................................................................................
// about page ......................................................................................
function load_about_content(){
	var markup = about;
	$('.list').html(markup);
}





// .................................................................................................
// speaker list.....................................................................................
function load_speaker_content(){
	var markup = '';
	for( var i=0; i<speakers.length; i++ ){
		if( data_page_detail == 'none' ){
			// load speaker list
			markup += '<p><a href="#" data-page="speakers" data-page-detail="'+speakers[i]['slug']+'">' + speakers[i]['first_name'] + ' ' + speakers[i]['last_name'] + '</a>';	
			markup += '</p>';
		}else{
			// load speaker detail
			if( speakers[i]['slug'] == data_page_detail ){
				markup += '<h1>' + speakers[i]['first_name'] + ' ' + speakers[i]['last_name'] + '</h1>';
				markup += '<p>' + speakers[i]['speaker_bio'] + '</p>';
				for( var ii=0; ii<sessions.length; ii++ ){
					// get sessions presented by this speaker
					var count = 0;
					if( $.inArray( speakers[i]['id'], sessions[ii]['speakers'] ) != -1  ){
						count += 1;
						if( count == 1){
							markup += '<h2>Sessions</h2>';
						}
						markup += '<p><a href="#" data-page="sessions" data-page-detail="'+sessions[ii]['slug']+'">' + sessions[ii]['title'] + '</a>';
						markup += '<br>' + sessions[ii]['start_date'];
						markup += '<br>' + sessions[ii]['start_time'] + ' — ' + sessions[ii]['end_time'];
						markup += '</p>';
					}
				}
				markup += '<p><hr><a href="#" data-page="speakers" data-page-detail="none" >Back to Speakers</a></p>';
			}
		}
		$('.list').html(markup);
	}
}





// .................................................................................................
// sessions.........................................................................................
function load_session_content(){
	var markup = '';
	var day = '';
	for( var i=0; i<sessions.length; i++ ){
		if( data_page_detail == 'none' ){
			// load sessions in schedule format
			var new_day = sessions[i]['start_day'];
			if( new_day != day ){
				day = new_day;
				markup += '<h2>'+new_day+', '+ sessions[i]['start_date'] +'</h2>';
			}
			markup += '<p>';
			markup += '<a href="#" data-page="sessions" data-page-detail="'+sessions[i]['slug']+'"><strong>'+sessions[i]['title']+'</strong></a>';
			markup += '<br>' + sessions[i]['start_time'] + ' — ' + sessions[i]['end_time'];
			for( var ii=0; ii<sessions[i]['speakers'].length; ii++ ){
				for( var iii=0; iii<speakers.length; iii++ ){
					if( sessions[i]['speakers'][ii] == speakers[iii]['id'] ){
						markup += '<br>' + speakers[iii]['first_name'] + ' ' + speakers[iii]['last_name'];
					}
				}
			}
			markup += '</p>';
		}else{
			// load speaker detail
			if( sessions[i]['slug'] == data_page_detail ){
				markup += '<h1>' + sessions[i]['title'] + '</h1>';
				markup += '<br>' + sessions[i]['start_date'];
				markup += '<br>' + sessions[i]['start_time'] + ' — ' + sessions[i]['end_time'];
				markup += '</p>';
				var count = 0;
				for( var ii=0; ii<sessions[i]['speakers'].length; ii++ ){
					for( var iii=0; iii<speakers.length; iii++ ){
						if( sessions[i]['speakers'][ii] == speakers[iii]['id'] ){
							count += 1;
							if( count == 1){
								markup += '<h3>Speakers</h3>';
							}
							markup += '<p><a href="#" data-page="speakers" data-page-detail="'+speakers[iii]['slug']+'">' + speakers[iii]['first_name'] + ' ' + speakers[iii]['last_name'] + '</a></p>';
						}
					}
				}
				markup += '<p><hr><a href="#" data-page="sessions" data-page-detail="none" >Back to Schedule</a></p>';
			}
		}
	}
	$('.list').html(markup);
}





// .................................................................................................
// load gallery.....................................................................................
function load_gallery_content(){
	var markup = '';
	for( var i=0; i<gallery.length; i++ ){
		markup += '<span class="thumbnail" data-index="'+i+'"><img src="' + gallery[i]['thumbnail'] + '"></span>';
		$('.list').html(markup);
	}
	$('.list span').off('touchstart click').on('touchstart click',function(e){
		e.stopPropagation();
		e.preventDefault();
		var selected_item = parseInt($(this).attr('data-index'));
		activate_gallery(selected_item);
		//console.log('selected_item = ' + selected_item);
	});
}





// .................................................................................................
// activate gallery - initialie photoswipe..........................................................
function activate_gallery(selected_item){
	var pswpElement = document.querySelectorAll('.pswp')[0];
	var options = {
		index: selected_item
	};
	var project_gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, gallery, options);
	project_gallery.init();
}





// .................................................................................................
// unload and load content..........................................................................
function change_view(){
	
	/* DEBUG */ $('#debug').html(data_page + ', ' + data_page_detail);
	
	$('.list').animate({'opacity':0},400,function(){
		$('.list').html('');
		switch(data_page){
			case 'home':
				load_home_content();
				break;
			case 'about':
				load_about_content();
				break;
			case 'speakers':
				load_speaker_content();
				break;
			case 'sessions':
				load_session_content();
				break;
			case 'gallery':
				load_gallery_content();
				break;
		}
		$('.list').animate({'opacity':1},400);
		activate_links();
		//history.pushState(data_page,data_page,data_page);
	});
}





// .................................................................................................
// activate newly injected links....................................................................
function activate_links(){
	$('.list a').off('touchstart click').on('touchstart click',function(e){
		e.stopPropagation();
		e.preventDefault();
		data_page        = $(this).attr('data-page');
		data_page_detail = $(this).attr('data-page-detail');
		change_view();
	});
}





$(document).ready(function(){

	// inject PhotoSwipe gallery markup
	$('body').append(gallery_markup);
	
	// load data
	$.getJSON(data_url, function(data){
		loaded_data = data;
		home     = loaded_data['page_home'];
		about    = loaded_data['page_about'];
		footer   = loaded_data['footer'];
		speakers = loaded_data['speakers'];
		sessions = loaded_data['sessions'];
		gallery  = loaded_data['gallery'];
		change_view();
	});

	// activate main navigation
	$('nav a[data-page]').on('click',function(e){
		e.preventDefault();
		data_page = $(this).attr('data-page');
		//data_page_detail = $(this).attr('data-slug');
		data_page_detail = 'none';
		change_view();
	});

});




